from importlib import import_module
from django.test import TestCase, Client
from django.conf import settings
from django.http import HttpRequest
from django.urls import resolve

from . views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class BukuUnitTest(TestCase):

    #URL Test
    def test_url_page_found(self):
        response = Client().get('/buku/')
        self.assertEqual(response.status_code, 200)

    def test_url_page_not_found(self):
        response = Client().get('/halo/')
        self.assertEqual(response.status_code, 404)

    #Function Test
    def test_page_using_index_func(self):
        found = resolve('/buku/')
        self.assertEqual(found.func, index)

    #Template test
    def test_page_use_template(self):
        response = Client().get('/buku/')
        self.assertTemplateUsed(response, 'buku.html')

    #Text HTML Test
    def test_landing_page(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("FIND THE BOOKS", html_response)

class BukuFunctionTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(BukuFunctionTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(BukuFunctionTest, self).setUp()

    #Test Function Search
    def test_search_is_working(self):
        selenium = self.selenium
        selenium.get("http://localhost:8000/buku/")

        selenium_search = selenium.find_element_by_id('search')
        selenium_btn = selenium.find_element_by_class_name('btn')
        selenium_search.send_keys('Harry Potter')
        selenium_btn.click()
        time.sleep(5)