from django.shortcuts import render, redirect
from .models import Cari
from .forms import CariBuku

def index(request):
    context = {
        'page_title':'buku',
    }

    return render(request, 'buku.html', context)