from django import forms

from .models import Cari

class CariBuku(forms.ModelForm):
    cari_buku = forms.CharField(max_length=300)

    class Meta:
        model = Cari
        fields = [
            'cari_buku',
        ]

        widgets = {
            'cari_buku' : forms.Textarea(
                attrs = {
                    'id' : 'id_input',
                    'class' : 'form-control',
                    'placeholder' : 'Search',
                    'cols' : 50,
                    'row' : 5,
                }
            )
        }