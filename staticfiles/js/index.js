$(document).ready(function() {

    $(".change").click(function() {
        $(".accordion").toggleClass("accordion-secondary-theme");
        $("body").toggleClass("secondary-theme");
        $("p").toggleClass("p-secondary-theme");
        $("h1").toggleClass("h1-secondary-theme");
    });

    //Membuka dan menutup accordion
    var acc = document.getElementsByClassName("accordion")
    var i;
    
    for(i = 0; i < acc.length; i++){
        acc[i].addEventListener("click",function(){
            
            /* toggle between adding and removing the active class*/
            this.classList.toggle("active");
    
            /* toggle between hiding and showing the active panel */
            var panel = this.nextElementSibling;
            if(panel.style.display == "block"){
                panel.style.display = "none";
            }
            else{
                panel.style.display = "block";
            }
        });
    }
});

