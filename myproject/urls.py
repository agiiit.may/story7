from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('logout/', views.logoutView, name='logout'),
    path('rahasia/logout/', views.logoutView, name='rahasia_logout'),
    path('login/', views.loginView, name='login'),
    path('rahasia/login/', views.loginView, name='rahasia_login'),
    path('rahasia/', views.index, name='index'),
    path('admin/', admin.site.urls),
    path('', include('blog.urls')),
    path('buku/', include('buku.urls')),
]
