from importlib import import_module
from django.test import TestCase, Client
from django.conf import settings
from django.http import HttpRequest
from django.urls import resolve
from django.contrib.auth import authenticate, login, logout


from . views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class BlogUnitTest(TestCase):

    #URL test
    def test_url_page_found(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_page_not_found(self):
        response = Client().get('/halo/')
        self.assertEqual(response.status_code, 404)

    #Function test
    def test_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    #Template test
    def test_page_use_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index_anonymous.html')

    #TEXT HTML TEST
    # def test_landing_page(self):
    #     request = HttpRequest()
    #     engine = import_module(settings.SESSION_ENGINE)
    #     request.session = engine.SessionStore(None)
    #     response = index(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn("MUHAMAD AGUNG YULIANANG", html_response)

class BlogFunctiontest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(BlogFunctiontest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(BlogFunctiontest, self).tearDown()

    # TEST FUNCTION OF ACCORDION
    def test_accordion_is_working(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')

        response = selenium.get('http://localhost:8000/')

        activity_accordion = selenium.find_element_by_class_name("accordion")
        activity_accordion.click()
        time.sleep(1)

    def test_change_theme_is_work(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')

        response = selenium.get('http://localhost:8000/')

        button = selenium.find_element_by_class_name("change")
        button.click()
        time.sleep(2)