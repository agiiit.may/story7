from django.shortcuts import render, redirect
from django.contrib.auth import authenticate

def index(request):
    context = {
        'page_title':'Biodata'
    }

    template_name = None
    if request.user.is_authenticated:
        # jika sudah ada user yang authenticate
        template_name = 'index.html'
    else:
        # untuk anonymous user
        template_name = 'index_anonymous.html'

    return render(request, template_name, context)
